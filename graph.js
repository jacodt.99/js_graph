
export default class Graph{

    constructor(nNodi){
        this._dim = nNodi;
        this._adjMatrix = {};
    }

    //get methods
    getDim(){ return this._dim; }
    getAdjMatrix(){ return this._adjMatrix; }

    addArc(u, v, w){
        this._adjMatrix[u][v] = w;
    }
    addEdge(u, v, w){
        this.addArc(u, v, w);
        this.addArc(v, u, w);
        
    }
    removeArc(u, v){
        this._adjMatrix[u][v] = 0;
    }
    removeEdge(u, v){
        this.removeArc(u, v);
        this.removeArc(v, u);
    }

    addNode(id){
        this._adjMatrix[id] = {};
        Object.keys(this._adjMatrix).forEach((key) => {
            this._adjMatrix[id][key] = 0;
            this._adjMatrix[key][id] = 0;
        });
        this._dim++;
    }
    removeNode(id){
        Object.keys(this._adjMatrix).forEach((key) => {
            delete this._adjMatrix[key][id];
        });
        delete this._adjMatrix[id];
        this._dim--;
    }


    print(){
        console.log(this._adjMatrix);
    }

    isComplete(){
        var res = true;
        Object.keys(this._adjMatrix).forEach((rowKey) => {
            Object.keys(this._adjMatrix[rowKey]).forEach((colKey) => {
                if(this._adjMatrix[rowKey][colKey] === 0 && colKey !== rowKey)
                    res = false;
            });
        });
        return res;
    }
    isConnected(){
        var found;
        var keys = Object.keys(this._adjMatrix);        
        for(var i = 0; i < keys.length; i++){
            found = false;
            for(var j = 0; j < keys.length; j++){
                console.log([keys[i]],[keys[j]]);
                console.log(this._adjMatrix[keys[i]][keys[j]])
                if (this._adjMatrix[keys[i]][keys[j]] !== 0 || 
                    this._adjMatrix[keys[j]][keys[i]] !== 0)
                    found = true;
            }
            if(!found) 
                return false;
        }
        return true;
    }

    isBiparted(){
        var keys = Object.keys(this._adjMatrix);
        var color = {};
        var queue = {};
        queue.push(keys[0]);
        while(queue.length !== 0){
            var u = queue.shift();
            for(var j = 0; j < keys.length; j++){
                var v = keys[j];
                if(this._adjMatrix[u][v] && color[v] === undefined){
                    console.log(u + " " + v);
                    color[v] = (color[u]) ? 0 : 1;
                    queue.push(v);
                }
                if(this._adjMatrix[u][v] && color[v] === color[u])
                    return false;
            }   
        }
        return true;
    }

    getEdgesType(){
        var info = {
            pre: {},
            post: {},
            time: 0
        }
        var type = {};
        var keys = Object.keys(this._adjMatrix);
        for(var i = 0; i < keys.length; i++){
            var u = keys[i];
            if(info.pre[u] === undefined){
                type[u] = {};
                this.edgeClassify(info, u, type)
            }
        }
        return type;
    }

    edgeClassify(info, u, type){

        info.time++;
        info.pre[u] = info.time;

        var keys = Object.keys(this._adjMatrix);
        for(var i = 0; i < keys.length; i++){
            var v = keys[i];
            if(this._adjMatrix[u][v] !== 0){
                if(info.pre[v] === undefined)
                    type[u][v] = "tree";
                else{
                    if(info.pre[v] < info.pre[u] && info.post[v] === undefined)
                        type[u][v] = "back";
                    else if(info.pre[v] > info.pre[u] && info.post[v] !== undefined)
                        type[u][v] = "forward";
                    else
                        type[u][v] = "cross";
                }
            }
                    
        }
        info.time++;
        info.post[u] = info.time;
    }

    prim(s){
        var keys = Object.keys(this._adjMatrix);
        var parents = {}; 
        var weights = {}; 
        var done = {}; 
        for (var i = 0; i < keys.length; i++){
            let v = keys[i];
            weights[v] = Infinity;
            done[v] = false;
        } 
     
        weights[s] = 0; 
        parents[s] = -1; 
     
        for (var count = 0; count < keys.length - 1; count++){

            var u = this._minKey(weights, done);
            if (u === undefined) continue;
            done[u] = true; 
     
            for (var i = 0; i < keys.length; i++) {
                let v = keys[i];
                if (this._adjMatrix[u][v] !== 0 && !done[v] && this._adjMatrix[u][v] < weights[v]){
                    parents[v] = u;
                    weights[v] = this._adjMatrix[u][v];
                }
            } 
        }
        return parents;
    }

    dijkstra(s){
        var keys = Object.keys(this._adjMatrix);
        var parents = {}; 
        var weights = {}; 
        var done = {}; 
        for (var i = 0; i < keys.length; i++){
            let v = keys[i];
            weights[v] = Infinity;
            done[v] = false;
        } 
     
        weights[s] = 0; 
        parents[s] = -1; 
     
        for (var count = 0; count < keys.length - 1; count++){

            var u = this._minKey(weights, done);
            if (u === undefined) continue;
            done[u] = true; 
     
            for (var i = 0; i < keys.length; i++) {
                let v = keys[i];
                if (this._adjMatrix[u][v] !== 0 && !done[v] && this._adjMatrix[u][v] + weights[u] < weights[v]){
                    parents[v] = u;
                    weights[v] = this._adjMatrix[u][v] + weights[u];
                }
            } 
        }
        return parents;
    }

    _minKey(weights, done){ 
        var min = Infinity;
        var minIndex; 
        
        var keys = Object.keys(this._adjMatrix);
        
        for (var i = 0; i < keys.length; i++){
            var v = keys[i];
            if (!done[v] && weights[v] < min){
                min = weights[v];
                minIndex = v; 
                console.log(v)
            }
        }
    
        return minIndex; 
    }

    invert(){
        var newAdjMatrix = {};
        var keys = Object.keys(this._adjMatrix);
        keys.forEach(rowKey => {
            var row = {};
            newAdjMatrix[rowKey] = row;
            keys.forEach(colKey => {
                newAdjMatrix[rowKey][colKey] = this._adjMatrix[colKey][rowKey];
            }); 
        });
        this._adjMatrix = newAdjMatrix;
    }

    complete(){
        var newAdjMatrix = {};
        var keys = Object.keys(this._adjMatrix);
        keys.forEach(rowKey => {
            var row = {};
            newAdjMatrix[rowKey] = row;
            keys.forEach(colKey => {
                if(rowKey !== colKey)
                    newAdjMatrix[rowKey][colKey] = 1;
                else
                    newAdjMatrix[rowKey][colKey] = 0;
            }); 
        });
        this._adjMatrix = newAdjMatrix;
    }

}