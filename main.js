import Graph from "./graph.js";



// control canvas objects creation and animation
class Display{
    constructor(ctx){
        this._ctx = ctx;
        this.settings = {
            canvas:{ 
                node:{
                    lineWidth: 1,
                    selectedLineWidth: 4,
                    lineColor: "#e3e3e3",
                    fontColor: "#e3e3e3",
                    font: "30px Monospace",
                    radius: 40
                },
                edge:{
                    lineWidth : 1,
                    selectedLineWidth : 4,
                    selectedLineColor: "#7234cf",
                    lineColor: "#e3e3e3",
                    fontColor: "#e3e3e3",
                    font: "25px Monospace",
                }
            },
            notifyPopupDuration : 1500,
        };
    }

    drawNode(x, y, id, selected){

        this._ctx.strokeStyle = this.settings.canvas.node.lineColor;
        this._ctx.font = this.settings.canvas.node.font;
        this._ctx.fillStyle = this.settings.canvas.node.fontColor;
        
        if(selected)
            this._ctx.lineWidth = this.settings.canvas.node.selectedLineWidth;
        else
            this._ctx.lineWidth = this.settings.canvas.node.lineWidth;
        
        this._ctx.beginPath();
        this._ctx.arc(x, y, this.settings.canvas.node.radius, 0, 2 * Math.PI);


        this._ctx.fillText(id, x - 9 * (id.toString().length) , y + 11);
        this._ctx.stroke();
    }

    drawEdge(node1Pos, node2Pos, selected, pseudoSelected, oriented, weight, weighted, oppositeArc){
        
        this._ctx.fillStyle = this.settings.canvas.edge.fontColor;
        this._ctx.strokeStyle = this.settings.canvas.edge.lineColor;
        this._ctx.lineWidth = this.settings.canvas.edge.lineWidth;
        // roba trigonometrica per disegnare gli archi
        var a = node2Pos.x - node1Pos.x;
        var b = node2Pos.y - node1Pos.y;
        //var rho = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        if (a > 0) var theta = Math.atan(b / a);
        else if (a < 0) 
            if (b >= 0) var theta = Math.atan(b / a) + Math.PI;
            else var theta = Math.atan(b / a) - Math.PI;
        else
            if(b > 0) var theta = Math.PI / 2;
            else var theta = - Math.PI / 2;

        if(oppositeArc && oriented){

            var x1 = (this.settings.canvas.node.radius) * Math.cos(theta + Math.PI / 16);
            var y1 = (this.settings.canvas.node.radius) * Math.sin(theta + Math.PI / 16);
            var x2 = (this.settings.canvas.node.radius) * Math.cos(theta - Math.PI / 16);
            var y2 = (this.settings.canvas.node.radius) * Math.sin(theta - Math.PI / 16);
        }
        else{     
                   
            var x1 = x2 = (this.settings.canvas.node.radius) * Math.cos(theta);
            var y1 = y2 = (this.settings.canvas.node.radius) * Math.sin(theta);
        }

        if(selected)
            this._ctx.lineWidth = this.settings.canvas.edge.selectedLineWidth;          
        else if (pseudoSelected){
            this._ctx.strokeStyle = this.settings.canvas.edge.selectedLineColor;
            this._ctx.fillStyle = this.settings.canvas.edge.selectedLineColor;
        }

        this._ctx.beginPath();
        if(oriented)
            this.drawArrow(node1Pos.x + x1, node1Pos.y + y1, node2Pos.x - x2, node2Pos.y - y2);
        else{
            this._ctx.moveTo(node1Pos.x + x1, node1Pos.y + y1);
            this._ctx.lineTo(node2Pos.x - x2, node2Pos.y - y2);
        }

        if(weighted){

            y1 -= 10;
            if(oppositeArc && oriented && node1Pos.x < node2Pos.x){
                y1 += 50;
            }

            var pol = CartesianToPolar(node2Pos.x - x2 - (node1Pos.x - x1), node2Pos.y - y2 - (node1Pos.y - y1));
            var car = polarTocartesian(pol.theta, pol.rho / 2);

            this._ctx.save();

            this._ctx.translate(node1Pos.x + car.x , node1Pos.y + car.y);

            if(Math.abs(pol.theta) < Math.PI / 2)
                this._ctx.rotate(pol.theta);
            else
                this._ctx.rotate(pol.theta - Math.PI);

            this._ctx.translate(-(node1Pos.x + car.x), -(node1Pos.y + car.y));

            this._ctx.font = this.settings.canvas.edge.font;
            this._ctx.fillText(weight, node1Pos.x + car.x , node1Pos.y + car.y );
            this._ctx.restore();
        }

        this._ctx.stroke();
    }
    openOptions(node1, node2){
        if (node2 === undefined); // opzioni node
        else;                     // opzioni edge
    }

    drawArrow(fromx, fromy, tox, toy) {
        var headlen = 15; // length of head in pixels
        var dx = tox - fromx;
        var dy = toy - fromy;
        var angle = Math.atan2(dy, dx);
        this._ctx.moveTo(fromx, fromy);
        this._ctx.lineTo(tox, toy);
        this._ctx.stroke();
        this._ctx.beginPath();
        this._ctx.moveTo(tox, toy);
        this._ctx.lineTo(tox - headlen * Math.cos(angle - Math.PI / 6), toy - headlen * Math.sin(angle - Math.PI / 6));
        this._ctx.moveTo(tox, toy);
        this._ctx.lineTo(tox - headlen * Math.cos(angle + Math.PI / 6), toy - headlen * Math.sin(angle + Math.PI / 6));
      }

    drawMatrix(adjMatrix, nodes, edges, oriented){

        var table = document.getElementById("adjMatrixTable");
        table.innerHTML = "";

        Object.keys(adjMatrix).forEach((rowKey, i)=> {
             
            if(i === 0){
                var firstRow = document.createElement('tr');
                firstRow.appendChild(document.createElement('td'));
                Object.keys(adjMatrix).forEach((rowKey, i)=> {
                    var temp = document.createElement('th');
                    temp.id = temp.innerHTML = rowKey;
                    if(nodes[rowKey].selected)
                        temp.style.backgroundColor = "rgba(109, 38, 232, 0.30)";
                    firstRow.appendChild(temp);
                });
                table.appendChild(firstRow);
            }


            var row = document.createElement('tr');

            Object.keys(adjMatrix[rowKey]).forEach((colKey, j) => {

                var cell = document.createElement('td');
                if(j === 0){
                    var temp = document.createElement('th');
                    temp.id = temp.innerHTML = rowKey;
                    if(nodes[rowKey].selected)
                        temp.style.backgroundColor = "rgba(109, 38, 232, 0.30)";
                    row.appendChild(temp);
                }

                var edgeSel = false;
                for(var i = 0; i < edges.length; i++){
                    if(edges[i].node1 === rowKey && edges[i].node2 === colKey && edges[i].selected)
                        edgeSel = true;
                    else if(!oriented && edges[i].node2 === rowKey && edges[i].node1 === colKey && edges[i].selected)
                        edgeSel = true;
                }
                cell.id = rowKey.toString() + " " + colKey.toString();
                if(nodes[colKey].selected || nodes[rowKey].selected){
                    cell.style.backgroundColor = "rgba(109, 38, 232, 0.30)";
                }
                if(adjMatrix[rowKey][colKey] !== 0 && edgeSel)
                    cell.style.fontWeight = 400;

                cell.appendChild(document.createTextNode(adjMatrix[rowKey][colKey]));
                row.appendChild(cell);                
            });

            table.appendChild(row);
        }); 
        document.getElementById("tableWrapper").appendChild(table);

        if(Object.keys(adjMatrix).length > 1)
            document.getElementById("tableWrapper").style.display = "block";
        else
            document.getElementById("tableWrapper").style.display = "none";
    }

    displayNotification(type, mex){
        var notWrapper = document.getElementById("notificationWrapper");
        if(type === "error") notWrapper.setAttribute("class", "error");
        else if (type === "warning") notWrapper.setAttribute("class", "warning");
        else notWrapper.setAttribute("class", "success");
        document.getElementById("notificationWrapper").style.display = "block";
        document.getElementById("notificationLabel").innerHTML = mex;
        window.setTimeout(() => {
            document.getElementById("notificationWrapper").style.display = "none";
        }, this.settings.notifyPopupDuration);
    }
    
    displayInfo(counter, selected){

        if(counter === 1){
            document.getElementById("editItemWrapper").style.display = "block";
            document.getElementById("itemId").innerHTML = JSON.stringify(selected, null, 4);
        }
        else
            document.getElementById("editItemWrapper").style.display = "none";
    }

    updateNodeCounter(i){
        document.getElementById("nodeNumber").value = this.idCounter = i;
    }

    addButtonsSetNormalStyle(button){
        button.classList.remove("active");
    }
    addButtonsSetActiveStyle(button){
        button.classList.add("active");
    }    
    minimizeAdjMatrix(){
        document.getElementById("adjMatrixTable").style.display = "none";
        document.getElementById("minimizeButton").setAttribute("class", "maximizeButton");
    }
    maximizeAdjTable(){
        document.getElementById("adjMatrixTable").style.display = "block";
        document.getElementById("minimizeButton").setAttribute("class", "minimizeButton");
    }
    switchButton(button){
        button.classList.add("active");
        if(button.id === "orientedButton")
            document.getElementById("unorientedButton").classList.remove("active");
        else if(button.id === "unorientedButton")
            document.getElementById("orientedButton").classList.remove("active");
        else if(button.id === "weightedButton"){
            document.getElementById("unweightedButton").classList.remove("active");
            document.getElementById("edgeWeight").readOnly = false;
        }
        else if(button.id === "unweightedButton"){
            document.getElementById("weightedButton").classList.remove("active");
            document.getElementById("edgeWeight").value = 1;
            document.getElementById("edgeWeight").readOnly = true;
        }
    }
}


// interface between display and graph
class Middleware{
    constructor(display){
        this.display = display;
        this.UGraph = new Graph(0);
        this.OGraph = new Graph(0);
        this.nodes = {};
        this.edges = [];
        this.idCounter = 1;
        this.oriented = true;
        this.weighted = true;
    }
    createNode(x, y, id){
        this.UGraph.addNode(id);
        this.OGraph.addNode(id);
        this.nodes[id] = {pos:{x : x, y : y}, selected: false};
        this.refresh();
        this.displayAdjMatrix();
    }
    nodeOptions(node){
        
        display.refresh();
    }
    moveNode(node, originX, originY, x, y){
        this.nodes[node].pos.x += x - originX;
        this.nodes[node].pos.y += y - originY;
        this.refresh();
    }
    createEdge(u, v, w){
        var oppositeArc = false;

        if(!this.weighted) w = 1;

        this.OGraph.addArc(u, v, w);        
        this.UGraph.addEdge(u, v, w);
        
        this.edges.forEach((edge) => {
            if (u === edge.node2 && v === edge.node1){
                edge.oppositeArc = true;
                oppositeArc = true;
            }
        });

        this.edges.push({node1 : u, node2 : v, selected: false, oppositeArc: oppositeArc, weight: w});

        this.refresh();
        this.displayAdjMatrix();

    }
    edgeHolding(node1, node2){
        
        display.refresh();
    }
    edgeOptions(node1, node2){
        
        display.refresh();
    }
    delete(){
        for(var i = this.edges.length - 1; this.edges[i] !== undefined; i--){
            if(this.edges[i].selected)
                this.deleteEdge(i);
        }
        Object.keys(this.nodes).forEach((key) => {
            if(this.nodes[key].selected)        
                this.deleteNode(key);
        });
    }

    deleteNode(key){
        for(var j = this.edges.length - 1; j >= 0; j--){
            if(this.edges[j].node1 === key || this.edges[j].node2 === key){
                this.UGraph.removeEdge(this.edges[j].node1, this.edges[j].node2)
                this.OGraph.removeEdge(this.edges[j].node1, this.edges[j].node2)
                this.edges.splice(j, 1);
            }
        }
        this.UGraph.removeNode(key);                
        this.OGraph.removeNode(key);                
        delete this.nodes[key];

        this.calibrateDefaultNodeId();
        this.updateSelection();
        this.displayAdjMatrix();
        this.refresh();
    }

    deleteEdge(i){
        var found = false;
        if(this.edges[i].oppositeArc){
            for(var j = 0; j < this.edges.length; j++){
                if (this.edges[j].node1 === this.edges[i].node2 &&
                    this.edges[j].node2 === this.edges[i].node1){
                    if(this.oriented)
                        this.edges[j].oppositeArc = false;
                    else{
                        this.OGraph.removeArc(this.edges[j].node1, this.edges[j].node2);
                        this.edges.splice(j, 1);
                        found = true;
                    }
                    break;
                }
            }
        }

        i = (i >= j && found) ? (i - 1) : i;
        if(!this.edges[i].oppositeArc || !this.oriented)this.UGraph.removeEdge(this.edges[i].node1, this.edges[i].node2)
        this.OGraph.removeArc(this.edges[i].node1, this.edges[i].node2);
        this.edges.splice(i, 1);

        this.updateSelection();
        this.displayAdjMatrix();
        this.refresh();
    }

    displayAdjMatrix(){
        if(this.oriented)
            this.display.drawMatrix(this.OGraph.getAdjMatrix(), this.nodes, this.edges, this.oriented);
        else
            this.display.drawMatrix(this.UGraph.getAdjMatrix(), this.nodes, this.edges, this.oriented);
    }

    calibrateDefaultNodeId(){

        var i = 1;
        var max = 0;
        Object.keys(this.nodes).forEach(key => {
            if (parseInt(key) > parseInt(max))
                max = key;
        });
        while(this.nodes[i] !== undefined && i++ < max);

        this.display.updateNodeCounter(i);
    }

    idAlreadyExist(id){
        var res = false;
        Object.keys(this.nodes).forEach((key) => {
            if(id == parseInt(key)) 
                res = true;
        });
        return res;
    }

    notify(type, mex){
        this.display.displayNotification(type, mex);
    }

    updateSelection(){
        
        var selected = undefined;
        var counter = 0;

        Object.keys(this.nodes).forEach(key => {
            if(this.nodes[key].selected){
                counter++;
                selected = {type: "node", id: key};
            }
        });
        this.edges.forEach(edge => {
            if(edge.selected){
                counter++;
                selected = {type: "edge", u: edge.node1, v: edge.node2, weight: edge.weight};
            }
        });

        this.display.displayInfo(counter, selected);
        
    }
    
    refresh(){        
        this.display._ctx.clearRect(0, 0, canvas.width, canvas.height);
        this.edges.forEach((edge) => {
            var pseudoSelected = false;
            Object.keys(this.nodes).forEach((key) => {
                if(this.nodes[key].selected && (edge.node1 === key || edge.node2 === key))
                pseudoSelected = true;
                });
                this.display.drawEdge(this.nodes[edge.node1].pos, this.nodes[edge.node2].pos, edge.selected, pseudoSelected, this.oriented, edge.weight, this.weighted, edge.oppositeArc);
            });
            Object.keys(this.nodes).forEach((key) => {
            this.display.drawNode(this.nodes[key].pos.x, this.nodes[key].pos.y, key, this.nodes[key].selected);
        });
    }
    updateInfo(){
        
        console.log(this.UGraph.prim(6));
        if(this.oriented){
            //console.log(this.OGraph.getEdgesType());
            //console.log(this.OGraph.isComplete())
            //console.log(this.OGraph.isConnected());
        }
        else{
            //console.log("biparted: " + this.UGraph.isBiparted());
            //console.log(this.UGraph.isComplete())
            //console.log("connected: " + this.OGraph.isConnected());
            
        }
    }
    invertOGraph(){
        var newEdges = [];
        this.edges.forEach((e) => {
            newEdges.push({node1 : e.node2, node2 : e.node1, selected: e.selected, oppositeArc: e.oppositeArc, weight: e.weight});
        });
        this.edges = newEdges;
        this.OGraph.invert();
 
        this.updateSelection();
        this.refresh();
        this.displayAdjMatrix();
    }
    completeGraph(){
        var newEdges = [];
        Object.keys(this.nodes).forEach((node1) => {
            Object.keys(this.nodes).forEach((node2) => {
                if (node1 !== node2)
                    newEdges.push({node1 : node1, node2 : node2, selected: false, oppositeArc: true, weight: 1});
            });
        });
        this.edges = newEdges;
        this.OGraph.complete();
        this.UGraph.complete();

        this.updateSelection();
        this.refresh();
        this.displayAdjMatrix();
        
    }
}  


/*
gestisce gli input dell'utente e richiama le 
funzioni del middleware per interfacciarsi con
canvas e graph
*/
class Controller {
    
    constructor(middleware, canvas){

        // default settings
        this.mode = "normal";
        this.holding = false;
        this.movingOrigin = undefined;
        this.hittingEntity = { type: undefined, id: 1};
        this.moved = false;

        this.flags = {};        // .done  serve per gestire il ctrl per azioni multiple
        this.cursor = {};       // cursor position
        this.pressedKeys = {};  // pressedKey[keycode] = true se è premuto

        this.middleware = middleware;
        this.initListeners(canvas);
    }
    
    initListeners(canvas){

        //----------------------------------------//
        //-----------CANVAS-LISTENERS-------------//
        //----------------------------------------//
        canvas.addEventListener("mousemove", (event) => {
            this.adjustPointerCoords(canvas, event.clientX, event.clientY);
            this.updateHittingEntity();

            // gestione selezione e movimento nodi e edges 
            if(this.mode === "normal"){
                if (this.movingOrigin !== undefined){

                    if(this.movingOrigin.node !== 0){

                        if (!this.pressedKeys[17] && !this.middleware.nodes[this.movingOrigin.node].selected)
                            this.deselectAll();

                        this.middleware.nodes[this.movingOrigin.node].selected = true;
                        this.moved = true;

                        
                        Object.keys(this.middleware.nodes).forEach((key) => {
                            if (this.middleware.nodes[key].selected)
                                this.middleware.moveNode(key, this.movingOrigin.x, this.movingOrigin.y, this.cursor.x, this.cursor.y);
                        });
                        
                        if (!this.moved)
                            this.middleware.moveNode(this.movingOrigin.node, this.movingOrigin.x, this.movingOrigin.y, this.cursor.x, this.cursor.y);

                    }
                    else{
                        Object.keys(this.middleware.nodes).forEach(key => { 
                            this.middleware.moveNode(key, this.movingOrigin.x, this.movingOrigin.y, this.cursor.x, this.cursor.y);
                        });
                    }
                    this.movingOrigin.x = this.cursor.x;
                    this.movingOrigin.y = this.cursor.y;
                }
            }

            this.updateCursor();

        });

        canvas.addEventListener("mousedown", (event) => {
            
            this.updateHittingEntity();
            if(this.hittingEntity.type !== "edge")
                this.movingOrigin = {node: this.hittingEntity.id, x : this.cursor.x, y: this.cursor.y};

        });

        canvas.addEventListener("mouseup", (event) => {
            this.updateHittingEntity();

            //gestione selezione
            if(!this.pressedKeys[17] && !this.moved)
                this.deselectAll();
            
            if(this.mode === "normal"){
                if (this.hittingEntity.type === undefined){
                        this.selectionOrigin = this.cursor;
                }
                else{
                    if(this.hittingEntity.type === "node"){
                        if (this.middleware.nodes[this.hittingEntity.id].selected && this.pressedKeys[17] && !this.moved)
                            this.middleware.nodes[this.hittingEntity.id].selected = false;
                        else
                            this.middleware.nodes[this.hittingEntity.id].selected = true;
                    }
                    if(this.hittingEntity.type === "edge"){
                        if (this.middleware.edges[this.hittingEntity.id].selected && this.pressedKeys[17])
                            this.middleware.edges[this.hittingEntity.id].selected = false;
                        else
                            this.middleware.edges[this.hittingEntity.id].selected = true;
                    }
                }
            }
            this.middleware.refresh();
            this.middleware.displayAdjMatrix();
            this.middleware.updateSelection();


            if (this.mode === "addingNode"){
                if (this.hittingEntity.type !== "node"){
                    if(this.middleware.idAlreadyExist(document.getElementById("nodeNumber").value))
                        this.middleware.notify("error", "node id already exists");
                    else if (parseInt(document.getElementById("nodeNumber").value) > 999)
                        this.middleware.notify("error", "max node number is 999");
                    else{
                        if (document.getElementById("nodeNumber").value === "")
                            this.middleware.calibrateDefaultNodeId();
                        this.middleware.createNode(this.cursor.x, this.cursor.y, document.getElementById("nodeNumber").value);
                        this.middleware.calibrateDefaultNodeId();
                    }
                }
                this.flags.done = true;
            }
            else if (this.mode === "addingEdge"){
                if (this.hittingEntity.type === "node" && this.hittingEntity.id !== this.movingOrigin.node){
                    if(this.edgeAlreadyExist(this.movingOrigin.node, this.hittingEntity.id))
                        this.middleware.notify("error", "edge already exists");
                    else if (parseInt(document.getElementById("edgeWeight").value) > 999)
                        this.middleware.notify("error", "max weight is 999");
                    else{
                        if (document.getElementById("edgeWeight").value === "")
                            document.getElementById("edgeWeight").value = 1;
                        this.middleware.createEdge(this.movingOrigin.node, this.hittingEntity.id, document.getElementById("edgeWeight").value);
                    }
                }
                this.flags.done = true;
            }

            if (!this.pressedKeys[17] && this.flags.done){
                this.mode = "normal";
                this.flags.done = false;
                this.middleware.display.addButtonsSetNormalStyle(document.getElementById("addNode"));
                this.middleware.display.addButtonsSetNormalStyle(document.getElementById("addEdge"));
            }

            this.moved = false;
            this.movingOrigin = undefined;

            this.updateCursor();
        });
        
        canvas.addEventListener("contextmenu", (event) => {

        });

        //----------------------------------------//
        //-----------BUTTON-LISTENERS-------------//
        //----------------------------------------//
        document.getElementById("addNode").addEventListener("click", () => {
            if(this.mode === "addingNode"){
                this.mode = "normal";
                this.flags.done = false;
                this.middleware.display.addButtonsSetNormalStyle(event.currentTarget);
                return;
            }
            else if(this.mode === "addingEdge"){
                this.flags.done = false;
                this.middleware.display.addButtonsSetNormalStyle(document.getElementById("addEdge"));
            } 
            this.mode = "addingNode";
            this.middleware.display.addButtonsSetActiveStyle(event.currentTarget);
            
        });
        document.getElementById("addEdge").addEventListener("click", () => {
            if(this.mode === "addingEdge"){
                this.mode = "normal";
                this.flags.done = false;
                this.middleware.display.addButtonsSetNormalStyle(event.currentTarget);
                return;
            }
            else if(this.mode === "addingNode"){
                this.flags.done = false;
                this.middleware.display.addButtonsSetNormalStyle(document.getElementById("addNode"));
            } 
            this.mode = "addingEdge";
            this.middleware.display.addButtonsSetActiveStyle(event.currentTarget);
            
        });
        document.getElementById("orientedButton").addEventListener("click", () => {
                this.middleware.display.switchButton(event.currentTarget);
                this.middleware.oriented = true;
                this.middleware.displayAdjMatrix();
                this.middleware.refresh();
        });
        document.getElementById("unorientedButton").addEventListener("click", () => {
                this.middleware.display.switchButton(event.currentTarget);
                this.middleware.oriented = false;
                this.middleware.displayAdjMatrix();
                this.middleware.refresh();
        });
        document.getElementById("weightedButton").addEventListener("click", () => {
                this.middleware.display.switchButton(event.currentTarget);
                this.middleware.weighted = true;
                this.middleware.refresh();
                this.middleware.displayAdjMatrix();
                this.middleware.refresh();
        });
        document.getElementById("unweightedButton").addEventListener("click", () => {
                this.middleware.display.switchButton(event.currentTarget);
                this.middleware.weighted = false;
                this.middleware.refresh();
                this.middleware.displayAdjMatrix();
                this.middleware.refresh();
            
        });
        document.getElementById("minimizeButton").addEventListener("click", (event) => {
            if (document.getElementById("adjMatrixTable").style.display == "block")
                this.middleware.display.minimizeAdjMatrix();
            else
                this.middleware.display.maximizeAdjTable();
            
        });
        document.getElementById("adjMatrixTable").addEventListener("click", (event) => {

            if(event.target.nodeName !== "TD")
                return;

            var node1 = event.target.id.split(" ")[0];
            var node2 = event.target.id.split(" ")[1];
            var adjMatrix;

            if(node1 === node2) 
                return;
            

            if(this.middleware.oriented) adjMatrix = this.middleware.OGraph.getAdjMatrix();
            else adjMatrix = this.middleware.UGraph.getAdjMatrix();

            if (adjMatrix[node1][node2] !== 0){
                for(var i = 0; i < this.middleware.edges.length; i++){
                    if(this.middleware.edges[i].node1 === node1 &&
                        this.middleware.edges[i].node2 === node2)
                        this.middleware.deleteEdge(i);
                    else if(this.middleware.edges[i].node1 === node2 &&
                        this.middleware.edges[i].node2 === node1 && 
                        !this.middleware.oriented)
                        this.middleware.deleteEdge(i);
                }
            }
            else if (!this.edgeAlreadyExist(node1, node2)){
                this.middleware.createEdge(node1, node2, 1);
            }
        });

        document.getElementById("invertButton").addEventListener("click", (e) => {
            this.middleware.invertOGraph();
        });
        document.getElementById("completeButton").addEventListener("click", (e) => {
            this.middleware.completeGraph();
        });

        //----------------------------------------//
        //-------------KEYS-LISTENERS-------------//
        //----------------------------------------//
        // (17 = ctrl)
        document.addEventListener("keydown", (e) => { this.pressedKeys[e.keyCode] = true; });

        document.addEventListener("keydown", (e) => { 
            if(e.keyCode ==  46)
                this.middleware.delete();
        });
        document.addEventListener("keyup", (e) => { 
            this.pressedKeys[e.keyCode] = false;
            if (e.keyCode == 17 && this.flags.done) {
                this.mode = "normal";
                this.middleware.display.addButtonsSetNormalStyle(document.getElementById("addNode"));
                this.middleware.display.addButtonsSetNormalStyle(document.getElementById("addEdge"));
                this.updateCursor();
            }
            // il controllo a flag.done serve in caso si prema ctrl, si clicchi un button e si rilasci ctrl
         });
         document.addEventListener("keyup", (e) => {
             if(e.keyCode == 27){
                this.mode = "normal";
                this.flags.done = false;
                this.middleware.display.addButtonsSetNormalStyle(document.getElementById("addNode"));
                this.middleware.display.addButtonsSetNormalStyle(document.getElementById("addEdge"));
                this.updateCursor();
             }
         });

    }

    updateCursor(){
        if(this.hittingEntity.type === "node" || this.hittingEntity.type === "edge")
            canvas.style.cursor = "pointer";
        else if(this.mode === "addingNode" || this.mode === "addingEdge")
            canvas.style.cursor = "copy";
        else if (this.movingOrigin !== undefined)
            canvas.style.cursor = "all-scroll";
        else
            canvas.style.cursor = "auto";
    }

    deselectAll(){
        Object.keys(this.middleware.nodes).forEach((key) => {
            this.middleware.nodes[key].selected = false;
        });
        this.middleware.edges.forEach((edge, i) => {
            edge.selected = false;
        });
    }

    updateHittingEntity(){
        this.hittingEntity = { type: undefined, id: 0};
        var width = 10;
        this.middleware.edges.forEach((edge, i) => {
            
        var a = this.middleware.nodes[edge.node1].pos.x;
        var b = this.middleware.nodes[edge.node1].pos.y;
            
        var node1Polar = CartesianToPolar(this.middleware.nodes[edge.node1].pos.x - this.middleware.nodes[edge.node2].pos.x, 
            this.middleware.nodes[edge.node1].pos.y - this.middleware.nodes[edge.node2].pos.y);
            
        var node2Polar = CartesianToPolar(this.middleware.nodes[edge.node2].pos.x - this.middleware.nodes[edge.node1].pos.x, 
            this.middleware.nodes[edge.node2].pos.y - this.middleware.nodes[edge.node1].pos.y);
        
        if(edge.oppositeArc && this.middleware.oriented){
            var cart1 = polarTocartesian(node1Polar.theta + Math.PI / 2 , 15);
            var cart2 = polarTocartesian(node1Polar.theta + Math.PI / 2 , 5);
            var cart3 = polarTocartesian(node2Polar.theta - Math.PI / 2 , 15);
            var cart4 = polarTocartesian(node2Polar.theta - Math.PI / 2 , 5);
        }
        else{
            var cart1 = polarTocartesian(node1Polar.theta - Math.PI / 2 , 10);
            var cart2 = polarTocartesian(node1Polar.theta - Math.PI / 2 , -10);
            var cart3 = polarTocartesian(node2Polar.theta - Math.PI / 2 , 10);
            var cart4 = polarTocartesian(node2Polar.theta - Math.PI / 2 , -10);
        }
        
        var p1 = { x:this.middleware.nodes[edge.node1].pos.x - cart1.x, y:this.middleware.nodes[edge.node1].pos.y - cart1.y };
        var p2 = { x:this.middleware.nodes[edge.node1].pos.x - cart2.x, y:this.middleware.nodes[edge.node1].pos.y - cart2.y };
        var p3 = { x:this.middleware.nodes[edge.node2].pos.x - cart3.x, y:this.middleware.nodes[edge.node2].pos.y - cart3.y };
        var p4 = { x:this.middleware.nodes[edge.node2].pos.x - cart4.x, y:this.middleware.nodes[edge.node2].pos.y - cart4.y };
        
        if(isInsideTriangle(this.cursor, p1, p3, p2) || isInsideTriangle(this.cursor, p3, p4, p2)){
            this.hittingEntity.type = "edge";
            this.hittingEntity.id = i;
        }
        });
        Object.keys(this.middleware.nodes).forEach((key) => {
            if (Math.sqrt((this.cursor.x - this.middleware.nodes[key].pos.x) * (this.cursor.x - this.middleware.nodes[key].pos.x) + 
                            (this.cursor.y - this.middleware.nodes[key].pos.y) * (this.cursor.y - this.middleware.nodes[key].pos.y)) < this.middleware.display.settings.canvas.node.radius){
                this.hittingEntity.type = "node";
                this.hittingEntity.id = key;
                
            }
        });
    }
    adjustPointerCoords(canvas, x, y){
        this.cursor.x = x - canvas.offsetLeft + window.pageXOffset + canvas.scrollLeft;
        this.cursor.y = y - canvas.offsetTop + window.pageYOffset + canvas.scrollTop;
    }
    edgeAlreadyExist(node1, node2){

        var found = false;
        this.middleware.edges.forEach(edge => {
            if(this.middleware.oriented){
                if (edge.node1 === node1 && edge.node2 === node2) 
                    found = true;
            }
            else{
                if (edge.node1 === node1 && edge.node2 === node2 || 
                    edge.node2 === node1 && edge.node1 === node2) 
                    found = true;
                }
        });
        return found;
    }
}

function CartesianToPolar(a, b){
    //var rho = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    if (a > 0) var theta = Math.atan(b / a);
    else if (a < 0) 
        if (b >= 0) var theta = Math.atan(b / a) + Math.PI;
        else var theta = Math.atan(b / a) - Math.PI;
    else
        if(b > 0) var theta = Math.PI / 2;
        else var theta = - Math.PI / 2
    
    var rho = Math.sqrt(Math.pow(a,2) + Math.pow(b, 2));
    return {theta: theta, rho: rho};
}

function polarTocartesian(theta, rho){
    return {x: rho * Math.cos(theta), y: rho * Math.sin(theta)};
}

function isInsideTriangle(s, a, b, c)
{
    var as_x = s.x - a.x;
    var as_y = s.y - a.y;
    var s_ab = (b.x - a.x) * as_y - (b.y - a.y) * as_x > 0;

    if((c.x - a.x) * as_y - (c.y - a.y) * as_x > 0 == s_ab) return false;
    if((c.x - b.x) * (s.y - b.y) - (c.y - b.y) * (s.x - b.x) > 0 != s_ab) return false;

    return true;
}

function initCanvas(){
    var canvas = document.getElementById("canvas");
    
    function resizeCanvas() {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    
    resizeCanvas();
    window.addEventListener("resize", () => {
        resizeCanvas();
        middleware.refresh();
    });
    return canvas;
}

var canvas = initCanvas();
var ctx = canvas.getContext("2d");

var display = new Display(ctx);
var middleware = new Middleware(display);
var controller = new Controller(middleware, canvas);